﻿using System.Collections.Generic;

namespace PollBall.Services
{
    public enum SelectedGame
    {
        Basketball, Football, Soccer, Volleyball, Biliard, Hockey, Golf, Tennis
    }

    public interface IPollResultsService
    {
        void AddVote(SelectedGame game);

        SortedDictionary<SelectedGame, int> GetVoteResult();
    }

    public class PollResultsService : IPollResultsService
    {
        private Dictionary<SelectedGame, int> _selectionVotes;

        public PollResultsService()
        {
            _selectionVotes = new Dictionary<SelectedGame, int>();
        }

        public void AddVote(SelectedGame game)
        {
            if (_selectionVotes.ContainsKey(game))
            {
                _selectionVotes[game] += 1;
            }
            else
            {
                _selectionVotes.Add(game, 1);
            }
        }

        public SortedDictionary<SelectedGame, int> GetVoteResult()
        {
            return new SortedDictionary<SelectedGame, int>(_selectionVotes);
        }
    }
}